/** 
 * This main class will start the program that skips rocks.
 * @author Zach
 *
 */
import java.util.Scanner;
public class Main {
/**
 * The main method that launches our program
 * @param args The command line argument you hardly ever see
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to the rock skipping program");
		
		System.out.println("How far can you throw a rock?");
		
		Scanner scanner = new Scanner (System.in);
		
		int distanceSkipped;
		
		boolean isDoneGettingInput = false;
		
		while(isDoneGettingInput==false){
			try{
			distanceSkipped=scanner.nextInt();
			isDoneGettingInput = true;
			System.out.println("You skipped your rock " +distanceSkipped+ " miles");
			}
			catch(Exception e){
				System.out.println("You need an integer value");
				scanner = new Scanner (System.in);
			}
		}
		
		System.out.println("You're going to eat an apple lol");
		Apple redDelicious = new Apple();
		System.out.println(redDelicious.toString());
		
		System.out.println("You're now leaving this program");
		
	}

}
